package com.example.mehrnahid.CustomeViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class CustomeTextView extends AppCompatTextView {


    public CustomeTextView(Context context) {
        super(context);
        init();
    }

    public CustomeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/b_koodak_bold.ttf");
            setTypeface(tf);
        }
    }
}
