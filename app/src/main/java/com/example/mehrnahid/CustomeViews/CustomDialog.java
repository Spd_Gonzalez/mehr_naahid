package com.example.mehrnahid.CustomeViews;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.mehrnahid.Models.EventModel;
import com.example.mehrnahid.R;
import com.example.mehrnahid.Remote.Interface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomDialog extends Dialog {

    private List<String> stringList = new ArrayList<>();
    private Spinner spinner;
    private String returnValueEve = "همه";
    private String returnValueYear = "";
    private Dialog dialog;
    private TextView text;
    private ArrayAdapter<String> adapter;
    private List<EventModel> eves;
    private Context context;
    private EditText edtYearFilter;
    private OnMyDialogResult mDialogResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // same you have
    }


    public CustomDialog(@NonNull Context context) {
        super(context);
        getEves();
        dialog = new Dialog(context);
        setContentDialog();
        spinner = dialog.findViewById(R.id.spinnerDialog);
        text = dialog.findViewById(R.id.text_dialog);
        edtYearFilter = dialog.findViewById(R.id.edtFilterByYr);
        this.context = context;
        onClick();

    }

    public interface OnMyDialogResult{
        void finish(String eveFilter,String yearFilter);
    }

    private void getEves(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Interface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Interface api = retrofit.create(Interface.class);
        Call<List<EventModel>> call = api.getEves();
        call.enqueue(new Callback<List<EventModel>>() {
            @Override
            public void onResponse(Call<List<EventModel>> call, Response<List<EventModel>> response) {

                eves = response.body();
                stringList.add(0,"همه");
                for (int i = 1 ; i<=eves.size()-1;i++){
                    stringList.add(i,eves.get(i).getEve());
                    Log.e("Events","Events are : " + stringList.get(i));
                }

                adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item,stringList);
            }

            @Override
            public void onFailure(Call<List<EventModel>> call, Throwable t) {
                Log.e("Events","Error" + t.getMessage());
            }
        });
    }
    public void setDialogResult(OnMyDialogResult dialogResult){
        mDialogResult = dialogResult;
    }
    private void setContentDialog(){
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog);
    }


    public void showDialog(String msg){
        spinner.setAdapter(adapter);
        text.setText(msg);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                returnValueEve = stringList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                returnValueEve = "همه";
            }
        });

        dialog.show();
    }

    private void onClick(){
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(v -> {
            dialog.dismiss();
            if (mDialogResult !=null){
                returnValueYear = edtYearFilter.getText().toString();
                mDialogResult.finish(returnValueEve,returnValueYear);
            }
        });
    }
}
