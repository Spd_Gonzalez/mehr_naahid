package com.example.mehrnahid.Remote;

import com.example.mehrnahid.Models.EventModel;
import com.example.mehrnahid.Models.ImageModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Interface {

    String BASE_URL = "http://10.0.2.2/";

    @FormUrlEncoded
    @POST("returnImages.php")
    Call<List<ImageModel>> getAllImage(@Field("eve") String eve,@Field("year") String year);

    @POST("getEvents.php")
    Call<List<EventModel>> getEves();


}
