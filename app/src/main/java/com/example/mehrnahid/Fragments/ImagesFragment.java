package com.example.mehrnahid.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mehrnahid.Activities.MainActivity;
import com.example.mehrnahid.Adapters.ImageGridAdapters;
import com.example.mehrnahid.Models.ImageModel;
import com.example.mehrnahid.R;
import com.example.mehrnahid.Remote.Interface;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ImagesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ImagesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImagesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "filterEve";
    private static final String ARG_PARAM2 = "filterYear";

    // TODO: Rename and change types of parameters
    private String mParam1="همه";
    private String mParam2 ="";

    private OnFragmentInteractionListener mListener;
    private RecyclerView rc;
    private View view;
    private List<ImageModel> fakeList =new ArrayList<>();
    private List<ImageModel> images;
    public ImagesFragment() {
        // Required empty public constructor
    }


    private static ImagesFragment newInstance(String param1, String param2) {
        ImagesFragment fragment = new ImagesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    private void fakeData(){
        ImageModel imageModel = new ImageModel("1",getResources().getDrawable(R.drawable.progress_animation).toString(),"asqar","tavallod","0");
        ImageModel imageMode2 = new ImageModel("2",getResources().getDrawable(R.drawable.progress_animation).toString(),"asqar","tavallod","0");
        ImageModel imageMode3 = new ImageModel("3",getResources().getDrawable(R.drawable.progress_animation).toString(),"asqar","tavallod","0");

        fakeList.add(imageModel);
        fakeList.add(imageMode2);
        fakeList.add(imageMode3);
    }
    private void injection(){
        fakeData();
        rc = view.findViewById(R.id.rcFragment);

    }
    private void setRecyclerView(){
        rc.setHasFixedSize(true);
        ImageGridAdapters adapters = new ImageGridAdapters(getContext(),fakeList);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),3);
        rc.setLayoutManager(layoutManager);
        rc.setAdapter(adapters);


    }
    private void getImages() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Interface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Interface api = retrofit.create(Interface.class);
        Call<List<ImageModel>> call = api.getAllImage(mParam1,mParam2);
        call.enqueue(new Callback<List<ImageModel>>() {
            @Override
            public void onResponse(Call<List<ImageModel>> call, Response<List<ImageModel>> response) {
                images = response.body();
                ImageGridAdapters adapters = new ImageGridAdapters(getContext(),images);
                rc.setAdapter(adapters);
                rc.getAdapter().notifyDataSetChanged();
                ((MainActivity) getActivity()).setRefreshVisibility(false);

            }

            @Override
            public void onFailure(Call<List<ImageModel>> call, Throwable t) {
                Log.d("error","Error" + t.getMessage());
                Toast.makeText(Objects.requireNonNull(getActivity()).getApplicationContext(),"اتصال اینترنت خودرا چک کنید",Toast.LENGTH_LONG).show();
                ((MainActivity) getActivity()).setRefreshVisibility(true);

            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_images,null);
        injection();
        setRecyclerView();
        getImages();
        return view;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
