package com.example.mehrnahid.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EventModel {
    @SerializedName("eve")
    @Expose
    private String eve;
    public EventModel(String eve){
        this.eve = eve;
    }
    public String getEve() {
        return eve;
    }

    public void setEve(String eve) {
        this.eve = eve;
    }
}
