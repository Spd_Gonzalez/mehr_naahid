package com.example.mehrnahid.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("imgUrl")
    @Expose
    private String imgUrl;
    @SerializedName("tinyImage")
    @Expose
    private String tinyImage;
    @SerializedName("eve")
    @Expose
    private String eve;
    @SerializedName("year")
    @Expose
    private String year;


    public ImageModel(String id, String imgUrl, String tinyImage, String eve, String year) {
        this.id = id;
        this.imgUrl = imgUrl;
        this.tinyImage = tinyImage;
        this.eve = eve;
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTinyImage() {
        return tinyImage;
    }

    public void setTinyImage(String tinyImage) {
        this.tinyImage = tinyImage;
    }

    public String getEve() {
        return eve;
    }

    public void setEve(String eve) {
        this.eve = eve;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
