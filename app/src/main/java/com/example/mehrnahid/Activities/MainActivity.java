package com.example.mehrnahid.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.airbnb.lottie.LottieAnimationView;
import com.example.mehrnahid.CustomeViews.CustomeButton;
import com.example.mehrnahid.CustomeViews.CustomDialog;
import com.example.mehrnahid.Fragments.AboutFrag;
import com.example.mehrnahid.Fragments.ImagesFragment;
import com.example.mehrnahid.Fragments.MainFrag;
import com.example.mehrnahid.R;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements CustomDialog.OnMyDialogResult{

    ChipNavigationBar bottomNavigation;
    Toolbar toolbar;
    CustomeButton btnCategoryShow;
    FrameLayout frameLayout;
    Context context;
    ArrayList<String> list;
    Activity activity;
    Boolean f = false;
    CustomDialog alert;
    String eveFilterValue = "همه";
    String yearFilterValue = "همه";
    LottieAnimationView animationView;

    private void injections(){
        toolbar = findViewById(R.id.toolbarMain);
        bottomNavigation = findViewById(R.id.bottomNav);
        frameLayout = findViewById(R.id.frame);
        btnCategoryShow = findViewById(R.id.btnCategory);
        activity = this;
        animationView = findViewById(R.id.lottieToolbar);
        alert = new CustomDialog(MainActivity.this);
        alert.setDialogResult(new CustomDialog.OnMyDialogResult(){
            public void finish(String eveFilter,String yearFilter){
                eveFilterValue = eveFilter;
                yearFilterValue = yearFilter;
                setEve(eveFilter);
                Bundle bundle = new Bundle();
                bundle.putString("filterEve", eveFilterValue );
                bundle.putString("filterYear", yearFilterValue );
                ImagesFragment fragInfo = new ImagesFragment();
                fragInfo.setArguments(bundle);
                setFragment(fragInfo);

            }
        });
        list = new ArrayList<>();
        list.add("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png");
        list.add("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png");
        list.add("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png");
        list.add("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png");
        list.add("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png");
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        StatusBarUtil.setColor(this,getResources().getColor(R.color.background_color));

    }

    public void setEve(String result){
        eveFilterValue = result;
    }


    public void setCustomeDialog(){

        btnCategoryShow.setOnClickListener(v -> {
            alert.showDialog("فیلتر کردن بر اساس : ");

        });
    }

    public void onAppStart(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted = prefs.getBoolean(getString(R.string.pref_previously_started), false);
        if(!previouslyStarted) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.pref_previously_started), Boolean.TRUE);
            edit.commit();
            Intent intent = new Intent(getApplicationContext(),IntroActivity.class);
            startActivity(intent);
        }

    }

    private void setBottomNavigation(){
        bottomNavigation.setItemSelected(R.id.menu_home,false);


    }

    public void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (fragment.getClass().getName().equals("com.example.mehrnahid.Fragments.ParentFragment")){
            f = true;
        }
        if (fragment.getClass().getName().equals("com.example.mehrnahid.Fragments.MainFrag")){
            f = false;
        }
        fragmentTransaction.replace(R.id.frame,fragment);
        fragmentTransaction.setCustomAnimations(R.anim.intro_fadein,R.anim.fade_out);
        fragmentTransaction.commit();

    }

    public void setFragmentFormfrag(Fragment fragment){
        setFragment(fragment);
        bottomNavigation.setItemSelected(R.id.mnu_images,false);

    }



    @Override
    public void onBackPressed() {
        if (f.equals(Boolean.TRUE)) {
            setFragment(new MainFrag());
            return;
        }
        else if (f.equals(Boolean.TRUE))
        {
            super.onBackPressed();
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }


    }
    public void setRefreshVisibility(Boolean visibility){
        if (visibility){
            animationView.setVisibility(View.VISIBLE);
            animationView.playAnimation();
        }
        if (!visibility){
            animationView.pauseAnimation();
            animationView.setAnimation(R.raw.repeat_reload_animation);
            animationView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onAppStart();
        setContentView(R.layout.activity_main);
        injections();
        context = getApplicationContext();

        setFragment(new MainFrag());
        setBottomNavigation();
        setCustomeDialog();
        bottomNavigation.setOnItemSelectedListener(i -> {
            switch (i){
                case R.id.menu_home:
                    setFragment(new MainFrag());
                    btnCategoryShow.setVisibility(View.GONE);
                break;
                case R.id.menu_about:
                    setFragment(new AboutFrag());
                    btnCategoryShow.setVisibility(View.GONE);
                break;
                case R.id.mnu_images
                        :setFragment(new ImagesFragment());
                        btnCategoryShow.setVisibility(View.VISIBLE);
                break;
            }
        });

        animationView.setOnClickListener(v -> {
            animationView.setAnimation(R.raw.colored_lottie_loading);
            animationView.setRepeatCount(3);
            animationView.playAnimation();
            alert = new CustomDialog(MainActivity.this);
            alert.setDialogResult((eveFilter, yearFilter) -> {
                eveFilterValue = eveFilter;
                yearFilterValue = yearFilter;
                setEve(eveFilter);
                Bundle bundle = new Bundle();
                bundle.putString("filterEve", eveFilterValue );
                bundle.putString("filterYear", yearFilterValue );
                ImagesFragment fragInfo = new ImagesFragment();
                fragInfo.setArguments(bundle);
                setFragment(fragInfo);

            });
            setFragment(new ImagesFragment());
        });



    }

    public void setFilterButtonVisible(){
        btnCategoryShow.setVisibility(View.VISIBLE);
    }
    @Override
    public void finish(String eveFilter,String yearFilter) {

    }
}
