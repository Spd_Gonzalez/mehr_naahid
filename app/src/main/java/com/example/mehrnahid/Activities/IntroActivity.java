package com.example.mehrnahid.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.example.mehrnahid.R;


public class IntroActivity extends AppCompatActivity {
    LottieAnimationView animationView;
    TextView txtTitle , txtDesc;
    Button btnVorod;
    Animation a;

    Runnable runnable1 = new Runnable() {
        @Override
        public void run() {
        btnVorod.clearAnimation();
        btnVorod.setAnimation(a);
        btnVorod.setVisibility(View.VISIBLE);
        }
    };
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            animationView.playAnimation();
            txtTitle.clearAnimation();
            txtTitle.startAnimation(a);
            txtDesc.clearAnimation();
            txtDesc.setAnimation(a);
            txtDesc.setVisibility(View.VISIBLE);
            txtTitle.setVisibility(View.VISIBLE);
            Handler handler = new Handler();
            handler.postDelayed(runnable1,750);
        }
    };
    private void setAnimationView(){
        animationView.setAnimation(R.raw.rainbow);
        animationView.setMaxFrame(18);
        animationView.setSpeed(1);
        Handler handler = new Handler();
        handler.postDelayed(runnable,1500);
    }

    private void injections(){
        animationView = findViewById(R.id.lottie);
        txtDesc = findViewById(R.id.txtDescription);
        txtTitle = findViewById(R.id.txtTitle);
        btnVorod = findViewById(R.id.btnIntro);

        txtDesc.setVisibility(View.INVISIBLE);
        txtTitle.setVisibility(View.INVISIBLE);
        btnVorod.setVisibility(View.INVISIBLE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        a = AnimationUtils.loadAnimation(this, R.anim.intro_fadein);
        a.reset();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        injections();
        setAnimationView();
        btnVorod.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
            this.finish();
        });



    }
}
