package com.example.mehrnahid.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mehrnahid.Models.ImageModel;
import com.example.mehrnahid.R;
import com.example.mehrnahid.Activities.SliderDemo;

import java.util.ArrayList;
import java.util.List;

public class ImageGridAdapters extends RecyclerView.Adapter<ImageGridAdapters.ViewHolder> {

    private Context context;
    private List<ImageModel> list;
    private LayoutInflater mInflater;
    private ArrayList<String>arrayList = new ArrayList<>();


    public ImageGridAdapters(Context context, List<ImageModel> list)
    {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.list = list;
        for (int i = 0 ; i<= list.size()-1;i++)
        {
            arrayList.add(i,list.get(i).getImgUrl());
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  mInflater.inflate(R.layout.single_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       Glide.with(context).load(list.get(position).getImgUrl()).placeholder(R.drawable.progress_animation).into(holder.img);
       holder.img.setOnClickListener(v -> {
           Intent intent = new Intent(context, SliderDemo.class);
           Bundle bundle = new Bundle();
           bundle.putStringArrayList("list",arrayList);
           intent.putExtra("pos",position);
           intent.putExtras(bundle);
           context.startActivity(intent);
       });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.singleImg);

        }
    }
}
